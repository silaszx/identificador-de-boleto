package com.example.games.identificadordeboletobancario;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final Map<String, String> BANCOS;
    static
    {
        BANCOS = new HashMap<String, String>();
        BANCOS.put("237", "Banco Bradesco S.A.");
        BANCOS.put("001", "Banco do Brasil S.A.");
        BANCOS.put("077", "Banco Itaú BBA S.A.");
        BANCOS.put("184", "Banco Inter S.A.");
        BANCOS.put("033", "Banco Santander (Brasil) S.A.");
    }
    TextView banco_output, moeda_output, cc_output, valor_output, vencimento_output;
    EditText boleto_input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        banco_output = (TextView) findViewById(R.id.banco_output);
        moeda_output = (TextView) findViewById(R.id.moeda_output);
        cc_output = (TextView) findViewById(R.id.cc_output);
        valor_output = (TextView) findViewById(R.id.valor_output);
        vencimento_output = (TextView) findViewById(R.id.vencimento_output);
        boleto_input = (EditText) findViewById(R.id.boleto_input);
    }

    public void ler_boleto(View v) {
        char[] chars_entrada =  boleto_input.getText().toString().toCharArray();
        if (chars_entrada.length == 45) {
            this.set_banco_output(chars_entrada);
            this.set_moeda_output(chars_entrada);
            this.set_cc_output(chars_entrada);
            this.set_vencimento_output(chars_entrada);
            this.set_valor_output(chars_entrada);
        } else {
            Toast.makeText(this , "boleto faltando dados", Toast.LENGTH_LONG);
        }

        startService(new Intent(MainActivity.this, BoletoService.class));
    }

    private void set_banco_output(char[] chars_entrada) {
        String result = "";
        for (int i = 0; i <= 2; i++ ) {
            result += String.valueOf(chars_entrada[i]);
        }
        result = BANCOS.get(result);
        if (result == null) {
            banco_output.setText("Invalido");
        } else {
            banco_output.setText(result);
        }

    }

    private void set_moeda_output(char[] chars_entrada) {
        String result;
        if (chars_entrada[3] == '9') {
            result = "Real";
        } else if (chars_entrada[3] == '0') {
            result = "Internacional";
        } else {
            result = "Invalida";
        }
        moeda_output.setText(result);
    }

    private void set_cc_output(char[] chars_entrada) {
        String result = "";
        for (int i = 4; i <= 8; i++ ) {
            result += String.valueOf(chars_entrada[i]);
        }
        cc_output.setText(result);
    }

    private void set_vencimento_output(char[] chars_entrada) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        String result = "";
        Calendar c = Calendar.getInstance();
        c.set(1997,10,07);

        for (int i = 29; i <= 33; i++ ) {
            result += String.valueOf(chars_entrada[i]);
        }

        c.add(Calendar.DATE, Integer.parseInt(result));
        vencimento_output.setText(sdf.format(c.getTime()));
    }

    private void set_valor_output(char[] chars_entrada) {
        String result = "";
        for (int i = 34; i <= 44; i++ ) {
            if(i == 42) { result += "."; }
            result += String.valueOf(chars_entrada[i]);
        }
        Float result_float=Float.parseFloat(result);
        valor_output.setText(String.format ("%,.2f", result_float));
    }
}
